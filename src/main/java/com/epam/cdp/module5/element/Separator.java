package com.epam.cdp.module5.element;

/**
 * User: Yevhen_Lobazov
 * Date: 11/27/13
 */
public class Separator extends Element {

    public Separator() {
        super(",", ElementType.SEPARATOR);
    }
}
