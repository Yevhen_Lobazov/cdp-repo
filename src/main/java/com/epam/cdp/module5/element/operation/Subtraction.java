package com.epam.cdp.module5.element.operation;

/**
 * User: Yevhen_Lobazov
 * Date: 11/18/13
 */
public class Subtraction extends BinaryOperation {

    public Subtraction() {
        super("-", OperatorPriority.ADDITION_SUBSTRACTION, Associativity.LEFT);
    }

    @Override
    protected double calculateBinary(double operand1, double operand2) {
        return operand1 - operand2;
    }
}
