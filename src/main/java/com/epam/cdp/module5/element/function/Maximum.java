package com.epam.cdp.module5.element.function;

import java.util.List;

/**
 * User: Yevhen_Lobazov
 * Date: 11/27/13
 */
public class Maximum extends ManyArgumentsFunction {

    public Maximum() {
        super("max");
    }

    @Override
    protected double calculateManyArguments(List<Double> arguments) {
        Double res = null;

        for (Double argument : arguments) {
            if (res == null || argument > res) {
                res = argument;
            }
        }

        return res;
    }
}
