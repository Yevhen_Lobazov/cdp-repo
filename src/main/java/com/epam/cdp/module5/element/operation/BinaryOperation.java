package com.epam.cdp.module5.element.operation;

import com.epam.cdp.module5.element.Element;
import com.epam.cdp.module5.element.Number;

import java.util.Stack;

/**
 * User: Yevhen_Lobazov
 * Date: 11/21/13
 */
public abstract class BinaryOperation extends Operation {

    protected BinaryOperation(String pattern, OperatorPriority operatorPriority, Associativity associativity) {
        super(pattern, operatorPriority, associativity);
    }

    @Override
    public double calculate(Stack<Element> elementStack) {
        Number operand2 = (Number) elementStack.pop();
        Number operand1 = (Number) elementStack.pop();

        return calculateBinary(operand1.getValue(), operand2.getValue());
    }

    protected abstract double calculateBinary(double operand1, double operand2);

}
