package com.epam.cdp.module5.element.operation;


import com.epam.cdp.module5.element.Element;
import com.epam.cdp.module5.element.Number;

import java.util.Stack;

/**
 * User: Yevhen_Lobazov
 * Date: 11/27/13
 */
public abstract class UnaryOperation extends Operation {

    protected UnaryOperation(String pattern, OperatorPriority operatorPriority, Associativity associativity) {
        super(pattern, operatorPriority, associativity);
    }

    @Override
    public double calculate(Stack<Element> elementStack) {
        return calculateUnary(((Number) elementStack.pop()).getValue());
    }

    protected abstract double calculateUnary(double operand);
}
