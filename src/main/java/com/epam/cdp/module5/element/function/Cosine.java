package com.epam.cdp.module5.element.function;

/**
 * User: Yevhen_Lobazov
 * Date: 11/27/13
 */
public class Cosine extends UnaryFunction {

    public Cosine() {
        super("cos");
    }

    @Override
    protected double calculateUnary(double argument) {
        return Math.cos(argument);
    }
}
