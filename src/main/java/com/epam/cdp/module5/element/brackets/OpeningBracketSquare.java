package com.epam.cdp.module5.element.brackets;

import com.epam.cdp.module5.element.Element;
import com.epam.cdp.module5.element.ElementType;

/**
 * User: Yevhen_Lobazov
 * Date: 11/27/13
 */
public class OpeningBracketSquare extends Element {

    public OpeningBracketSquare() {
        super("\\[", ElementType.OPENING_BRACKET);
    }
}
