package com.epam.cdp.module5.element.brackets;

import com.epam.cdp.module5.element.Element;
import com.epam.cdp.module5.element.ElementType;

/**
 * User: Yevhen_Lobazov
 * Date: 11/20/13
 */
public class ClosingBracketRound extends Element {

    public ClosingBracketRound() {
        super("\\)", ElementType.CLOSING_BRACKET);
    }
}
