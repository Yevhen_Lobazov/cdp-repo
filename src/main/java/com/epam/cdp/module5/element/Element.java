package com.epam.cdp.module5.element;

/**
 * User: Yevhen_Lobazov
 * Date: 11/20/13
 */
public abstract class Element{

    protected String pattern;

    protected ElementType type;

    protected Element(String pattern, ElementType type) {
        this.pattern = pattern;
        this.type = type;
    }

    public String getPattern() {
        return pattern;
    }

    public ElementType getType() {
        return type;
    }

}
