package com.epam.cdp.module5.element.operation;

/**
 * User: Yevhen_Lobazov
 * Date: 11/18/13
 */
public class Involution extends BinaryOperation {


    public Involution() {
        super("\\^", OperatorPriority.INVOLUTION, Associativity.RIGHT);
    }

    @Override
    protected double calculateBinary(double operand1, double operand2) {
        return Math.pow(operand1, operand2);
    }
}
