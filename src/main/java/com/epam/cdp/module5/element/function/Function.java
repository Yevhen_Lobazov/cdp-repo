package com.epam.cdp.module5.element.function;

import com.epam.cdp.module5.element.Calculable;
import com.epam.cdp.module5.element.Element;
import com.epam.cdp.module5.element.ElementType;

import java.util.Stack;

/**
 * User: Yevhen_Lobazov
 * Date: 11/26/13
 */
public abstract class Function extends Element implements Calculable {

    protected Function(String pattern) {
        super(pattern, ElementType.FUNCTION);
    }

    public abstract double calculate(Stack<Element> elementStack);
}
