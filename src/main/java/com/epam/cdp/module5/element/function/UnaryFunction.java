package com.epam.cdp.module5.element.function;

import com.epam.cdp.module5.element.*;
import com.epam.cdp.module5.element.Number;

import java.util.Stack;

/**
 * User: Yevhen_Lobazov
 * Date: 11/27/13
 */
public abstract class UnaryFunction extends Function {

    protected UnaryFunction(String pattern) {
        super(pattern);
    }

    @Override
    public double calculate(Stack<Element> elementStack) {
        return calculateUnary(((Number) elementStack.pop()).getValue());
    }

    protected abstract double calculateUnary(double argument);
}
