package com.epam.cdp.module5.element.operation;

/**
 * User: Yevhen_Lobazov
 * Date: 11/27/13
 */
public class Factorial extends UnaryOperation {

    public Factorial() {
        super("!", OperatorPriority.FACTORIAL, Associativity.RIGHT);
    }

    @Override
    protected double calculateUnary(double operand) {
        int n = (int) operand;
        double res = 1;
        for (int i = 2; i < n + 1; i++) {
            res *= i;
        }
        return res;
    }

}
