package com.epam.cdp.module5.element.function;


/**
 * User: Yevhen_Lobazov
 * Date: 11/26/13
 */
public class Sinus extends UnaryFunction {

    public Sinus() {
        super("sin");
    }

    @Override
    protected double calculateUnary(double argument) {
        return Math.sin(argument);
    }
}
