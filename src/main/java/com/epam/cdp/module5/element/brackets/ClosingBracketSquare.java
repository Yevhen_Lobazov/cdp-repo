package com.epam.cdp.module5.element.brackets;

import com.epam.cdp.module5.element.Element;
import com.epam.cdp.module5.element.ElementType;

/**
 * User: Yevhen_Lobazov
 * Date: 11/27/13
 */
public class ClosingBracketSquare extends Element {

    public ClosingBracketSquare() {
        super("\\]", ElementType.CLOSING_BRACKET);
    }
}
