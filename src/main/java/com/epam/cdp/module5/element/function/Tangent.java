package com.epam.cdp.module5.element.function;

/**
 * User: Yevhen_Lobazov
 * Date: 11/27/13
 */
public class Tangent extends UnaryFunction {

    public Tangent() {
        super("tg");
    }

    @Override
    protected double calculateUnary(double argument) {
        return Math.tan(argument);
    }


}
