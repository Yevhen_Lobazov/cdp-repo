package com.epam.cdp.module5.element;

import java.util.Stack;

/**
 * User: Yevhen_Lobazov
 * Date: 11/26/13
 */
public interface Calculable {

    double calculate(Stack<Element> elementStack);

}
