package com.epam.cdp.module5.element.operation;

/**
 * User: Yevhen_Lobazov
 * Date: 11/21/13
 */
public class UnaryMinus extends UnaryOperation {

    public UnaryMinus() {
        super("~", OperatorPriority.ADDITION_SUBSTRACTION, Associativity.RIGHT);
    }

    @Override
    protected double calculateUnary(double operand) {
        return 0 - operand;
    }
}
