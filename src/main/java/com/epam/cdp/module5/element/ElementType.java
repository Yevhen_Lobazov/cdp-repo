package com.epam.cdp.module5.element;

/**
 * User: Yevhen_Lobazov
 * Date: 11/20/13
 */
public enum ElementType {
    NUMBER, OPERATOR, OPENING_BRACKET, CLOSING_BRACKET, FUNCTION, SEPARATOR
}
