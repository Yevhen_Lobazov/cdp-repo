package com.epam.cdp.module5.element.function;

import com.epam.cdp.module5.element.Element;

import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

import com.epam.cdp.module5.element.Number;

import static com.epam.cdp.module5.util.CalculatorUtils.isNumber;

/**
 * User: Yevhen_Lobazov
 * Date: 11/27/13
 */
public abstract class ManyArgumentsFunction extends Function {

    protected ManyArgumentsFunction(String pattern) {
        super(pattern);
    }

    @Override
    public double calculate(Stack<Element> elementStack) {
        List<Double> arguments = new LinkedList<>();
        do {
            arguments.add(((Number) elementStack.pop()).getValue());
        }
        while (!elementStack.empty() && isNumber(elementStack.peek()));

        return calculateManyArguments(arguments);
    }

    protected abstract double calculateManyArguments(List<Double> arguments);
}
