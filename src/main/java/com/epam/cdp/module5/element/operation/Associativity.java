package com.epam.cdp.module5.element.operation;

/**
 * User: Yevhen_Lobazov
 * Date: 11/18/13
 */
public enum Associativity {
    LEFT, RIGHT
}
