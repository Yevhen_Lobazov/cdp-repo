package com.epam.cdp.module5.element.operation;

/**
 * User: Yevhen_Lobazov
 * Date: 11/20/13
 */
public enum OperatorPriority {
    ADDITION_SUBSTRACTION(10),
    MULTIPLICATION_DIVISION(20),
    INVOLUTION(30),
    FACTORIAL(40);

    private int value;

    private OperatorPriority(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}

