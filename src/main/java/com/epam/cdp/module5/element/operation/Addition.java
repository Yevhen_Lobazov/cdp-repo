package com.epam.cdp.module5.element.operation;

public class Addition extends BinaryOperation {


    public Addition() {
        super("\\+", OperatorPriority.ADDITION_SUBSTRACTION, Associativity.LEFT);
    }

    @Override
    protected double calculateBinary(double operand1, double operand2) {
        return operand1 + operand2;
    }

}
