package com.epam.cdp.module5.element.operation;


import com.epam.cdp.module5.element.Calculable;
import com.epam.cdp.module5.element.Element;
import com.epam.cdp.module5.element.ElementType;

import java.util.Stack;

public abstract class Operation extends Element implements Calculable {

    protected Integer priority;

    protected Associativity associativity;

    public Operation(String pattern, OperatorPriority operatorPriority, Associativity associativity) {
        super(pattern, ElementType.OPERATOR);
        this.priority = operatorPriority.getValue();
        this.associativity = associativity;
    }

    public Integer getPriority() {
        return priority;
    }

    public Associativity getAssociativity() {
        return associativity;
    }

    public abstract double calculate(Stack<Element> elementStack);
}
