package com.epam.cdp.module5.element.function;

/**
 * User: Yevhen_Lobazov
 * Date: 11/27/13
 */
public class Cotangent extends UnaryFunction {

    public Cotangent() {
        super("ctg");
    }

    @Override
    protected double calculateUnary(double argument) {
        return 1 / Math.tan(argument);
    }
}
