package com.epam.cdp.module5.element.brackets;

import com.epam.cdp.module5.element.Element;
import com.epam.cdp.module5.element.ElementType;

/**
 * User: Yevhen_Lobazov
 * Date: 11/20/13
 */
public class OpeningBracketRound extends Element {

    public OpeningBracketRound() {
        super("\\(", ElementType.OPENING_BRACKET);
    }
}
