package com.epam.cdp.module5.element;

/**
 * User: Yevhen_Lobazov
 * Date: 11/20/13
 */
public class Number extends Element {

    protected double value;

    public Number() {
        super("(\\d+\\.?\\d*|\\d*\\.\\d+)", ElementType.NUMBER);
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
}
