package com.epam.cdp.module5;

import com.epam.cdp.module5.calculation.Calculator;
import com.epam.cdp.module5.util.ParserException;

import java.math.BigDecimal;

/**
 * User: Yevhen_Lobazov
 * Date: 11/6/13
 */
public class Runner {

    public static void main(String[] args) {
        String input = System.getenv("input"); //getting input
        try {
            double result = Calculator.compute(input);
            result = BigDecimal.valueOf(result).setScale(2, BigDecimal.ROUND_HALF_DOWN).doubleValue();
            System.out.printf("%.2f", result);
        } catch (ParserException e) {
            System.out.println(e.getMessage());
        }
    }
}

