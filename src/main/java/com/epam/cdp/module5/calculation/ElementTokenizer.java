package com.epam.cdp.module5.calculation;

import com.epam.cdp.module5.element.*;
import com.epam.cdp.module5.element.Number;
import com.epam.cdp.module5.element.brackets.ClosingBracketRound;
import com.epam.cdp.module5.element.brackets.ClosingBracketSquare;
import com.epam.cdp.module5.element.brackets.OpeningBracketRound;
import com.epam.cdp.module5.element.brackets.OpeningBracketSquare;
import com.epam.cdp.module5.element.function.*;
import com.epam.cdp.module5.element.operation.*;
import com.epam.cdp.module5.util.ParserException;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * User: Yevhen_Lobazov
 * Date: 11/19/13
 */

public class ElementTokenizer {

    private static final List<Element> elements = new ArrayList<>();

    private String source;

    private int position;


    static {
        elements.add(new Number());
        elements.add(new OpeningBracketRound());
        elements.add(new ClosingBracketRound());
        elements.add(new OpeningBracketSquare());
        elements.add(new ClosingBracketSquare());
        elements.add(new Separator());

        //operators
        elements.add(new Addition());
        elements.add(new Subtraction());
        elements.add(new Multiplication());
        elements.add(new Division());
        elements.add(new Involution());
        elements.add(new UnaryMinus());
        elements.add(new Factorial());

        //functions
        elements.add(new Sinus());
        elements.add(new Cosine());
        elements.add(new Tangent());
        elements.add(new Cotangent());
        elements.add(new Minimum());
        elements.add(new Maximum());

    }

    public ElementTokenizer(String source) {
        this.source = prepareInputString(source);
    }

    public boolean isSourceEmpty() {
        return StringUtils.isBlank(source);
    }

    public Element getNextElement() throws ParserException {

        for (Element element : elements) {
            Pattern pattern = Pattern.compile("^" + element.getPattern());
            Matcher matcher = pattern.matcher(source);

            if (matcher.find()) {
                String captured = matcher.group();
                position += captured.length();
                source = source.substring(captured.length());

                if (element.getType() == ElementType.NUMBER) {
                    Number number = new Number();
                    number.setValue(Double.valueOf(captured));
                    return number;
                }
                return element;
            }
        }

        throw new ParserException("Unknown Element at position: " + position);
    }

    private String prepareInputString(String input) {
        if (StringUtils.isBlank(input)) {
            throw new IllegalArgumentException("Input string can't be blank");
        }

        input = input.replaceAll("\\s", "");
        input = input.replaceAll("(?<=\\[)[^,]+(?=,)", "($0)");
        input = input.replaceAll("(?<=,)[^,]+(?=,)", "($0)");
        input = input.replaceAll("(?<=,)[^,]+(?=\\])", "($0)");
        input = input.replaceAll("\\(-", "\\(~");


        if (input.charAt(0) == '-' && input.length() > 1) {
            input = "~" + input.substring(1);
        }

        return input;
    }

}
