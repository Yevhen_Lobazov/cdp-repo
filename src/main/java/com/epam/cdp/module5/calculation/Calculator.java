package com.epam.cdp.module5.calculation;


import com.epam.cdp.module5.element.*;
import com.epam.cdp.module5.element.Number;
import com.epam.cdp.module5.util.ParserException;

import java.util.List;
import java.util.Stack;

public class Calculator {

    public static Double compute(String input) throws ParserException {
        List<Element> elementsRpn = ReversePolishNotationParser.parseToElementList(input);
        Stack<Element> elementStack = new Stack<>();

        for (Element element : elementsRpn) {
            switch (element.getType()) {
                case NUMBER:
                    elementStack.push(element);
                    break;
                case OPERATOR:
                case FUNCTION:
                    Double res = ((Calculable) element).calculate(elementStack);
                    Number number = new Number();
                    number.setValue(res);
                    elementStack.push(number);
                    break;
            }
        }

        return ((Number) elementStack.pop()).getValue();
    }

}