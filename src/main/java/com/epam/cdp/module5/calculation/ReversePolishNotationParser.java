package com.epam.cdp.module5.calculation;

import com.epam.cdp.module5.element.operation.Associativity;
import com.epam.cdp.module5.element.Element;
import com.epam.cdp.module5.element.operation.Operation;
import com.epam.cdp.module5.util.ParserException;

import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

import static com.epam.cdp.module5.element.operation.Associativity.LEFT;
import static com.epam.cdp.module5.element.operation.Associativity.RIGHT;
import static com.epam.cdp.module5.util.CalculatorUtils.isFunction;
import static com.epam.cdp.module5.util.CalculatorUtils.isOpeningBracket;
import static com.epam.cdp.module5.util.CalculatorUtils.isOperator;

/**
 * User: Yevhen_Lobazov
 * Date: 11/22/13
 */
public class ReversePolishNotationParser {

    public static List<Element> parseToElementList(String input) throws ParserException {
        ElementTokenizer tokenizer = new ElementTokenizer(input);

        Stack<Element> operationStack = new Stack<>();
        List<Element> elementsRpn = new LinkedList<>();

        while (!tokenizer.isSourceEmpty()) {
            Element element = tokenizer.getNextElement();

            switch (element.getType()) {
                case NUMBER:
                    elementsRpn.add(element);
                    break;
                case OPENING_BRACKET:
                    operationStack.push(element);
                    break;
                case CLOSING_BRACKET:
                    processClosingBracket(operationStack, elementsRpn);
                    break;
                case OPERATOR:
                    processOperator(operationStack, elementsRpn, element);
                    break;
                case FUNCTION:
                    operationStack.push(element);
                    break;
                case SEPARATOR:
                    continue;
            }
        }

        while (!operationStack.empty()) {
            elementsRpn.add(operationStack.pop());
        }

        return elementsRpn;
    }

    private static void processOperator(Stack<Element> operationStack, List<Element> elementsRpn, Element element) {
        Associativity opAssociativity = ((Operation) element).getAssociativity();
        int opPriority = ((Operation) element).getPriority();

        while (!operationStack.empty()) {
            Element topStackElement = operationStack.peek();

            if (opAssociativity == RIGHT && isOperator(topStackElement) && opPriority < ((Operation) topStackElement).getPriority()) {
                elementsRpn.add(operationStack.pop());
            } else if (opAssociativity == LEFT && isOperator(topStackElement) && opPriority <= ((Operation) topStackElement).getPriority()) {
                elementsRpn.add(operationStack.pop());
            } else {
                break;
            }
        }
        operationStack.push(element);
    }

    private static void processClosingBracket(Stack<Element> operationStack, List<Element> elementsRpn) throws ParserException {
        while (true) {
            if (operationStack.empty()) {
                throw new ParserException("Some mess with separators or brackets aren't consistent");
            }
            Element stackElem = operationStack.pop();
            if (isOpeningBracket(stackElem)) {
                if (!operationStack.empty() && isFunction(operationStack.peek())) {
                    elementsRpn.add(operationStack.pop());
                }
                break;
            } else {
                elementsRpn.add(stackElem);
            }
        }
    }


}
