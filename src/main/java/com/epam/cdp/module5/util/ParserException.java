package com.epam.cdp.module5.util;

/**
 * User: Yevhen_Lobazov
 * Date: 11/18/13
 */
public class ParserException extends Exception {

    public ParserException(String message) {
        super(message);
    }

}
